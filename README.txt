WYSIWYG Youtube nocookie domain

-- SUMMARY --
Automatically replaces youtube.com embed's with youtube-nocookie.com domain.
Creating privacy on embeds.

-- REQUIREMENTS --

A WYSIWYG editor.

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.

-- CONFIGURATION --

* Go to your input formats (/admin/config/content/formats)
* Click the configure link next to the desired input format
* Click the checkbox next to 'Replace youtube.com embeds with youtube-nocookie.com'

